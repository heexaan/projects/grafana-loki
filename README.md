поднимает loki + grafana \
сменить пароль админа в docker-compose.yaml или указать в .env
```
git clone https://gitlab.com/heexaan/projects/grafana-loki.git
cd grafana-loki
chmod 777 -R ./grafana_data
chmod 777 -R ./loki_data
docker compsoe up -d
```
для отправки логов в loki, нужно установить docker plugin - https://grafana.com/docs/loki/latest/send-data/docker-driver/ \
отправка в локи при запуске контейнера
```
docker run --name container_name --log-driver=loki --log-opt loki-url=http://IP_ADDRESS_LOKI:3100/loki/api/v1/push docker-image:tag
```

для рандомной генерации логов можно использовать

```
docker run --name random-test --log-driver=loki --log-opt loki-url=http://192.168.1.108:3100/loki/api/v1/push chentex/random-logger:latest 100 400 100
```